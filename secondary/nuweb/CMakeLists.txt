# CMakeLists.txt file for nuweb

cmake_minimum_required(VERSION 2.8)

project(NUWEB)

set(NUWEB_VERSION 1.58)

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/bootstrap
  )

set(nuweb_bootstrap_SRCS
  bootstrap/main.c
  bootstrap/pass1.c
  bootstrap/latex.c
  bootstrap/html.c
  bootstrap/output.c
  bootstrap/input.c
  bootstrap/scraps.c
  bootstrap/names.c
  bootstrap/arena.c
  bootstrap/global.c
  )

add_executable(nuweb_bootstrap ${nuweb_bootstrap_SRCS})

set(nuweb_w_C_SRCS
  ${CMAKE_CURRENT_BINARY_DIR}/arena.c
  ${CMAKE_CURRENT_BINARY_DIR}/global.c
  ${CMAKE_CURRENT_BINARY_DIR}/html.c
  ${CMAKE_CURRENT_BINARY_DIR}/input.c
  ${CMAKE_CURRENT_BINARY_DIR}/latex.c
  ${CMAKE_CURRENT_BINARY_DIR}/main.c
  ${CMAKE_CURRENT_BINARY_DIR}/names.c
  ${CMAKE_CURRENT_BINARY_DIR}/output.c
  ${CMAKE_CURRENT_BINARY_DIR}/pass1.c
  ${CMAKE_CURRENT_BINARY_DIR}/scraps.c
  )

set(nuweb_w_SRCS
  ${nuweb_w_C_SRCS}
  ${CMAKE_CURRENT_BINARY_DIR}/global.h
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.1
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.tex
  )

add_custom_command(OUTPUT ${nuweb_w_SRCS}
  COMMAND nuweb_bootstrap
  ARGS -r ${CMAKE_CURRENT_SOURCE_DIR}/nuweb.w
  DEPENDS nuweb_bootstrap ${CMAKE_CURRENT_SOURCE_DIR}/nuweb.w
  COMMENT "[NUWEB] Tangling source file nuweb.w"
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

add_executable(nuweb ${nuweb_w_SRCS})

set(nuwebsty_w_SRCS
  ${CMAKE_CURRENT_BINARY_DIR}/nwhren.sty
  ${CMAKE_CURRENT_BINARY_DIR}/nwhres.sty
  ${CMAKE_CURRENT_BINARY_DIR}/nuwebsty.tex
  )

add_custom_command(OUTPUT ${nuwebsty_w_SRCS}
  COMMAND nuweb
  ARGS -r ${CMAKE_CURRENT_SOURCE_DIR}/nuwebsty.w
  DEPENDS nuweb ${CMAKE_CURRENT_SOURCE_DIR}/nuwebsty.w
  COMMENT "[NUWEB] Tangling source file nuwebsty.w"
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

set(nuweb_latex_SRCS
  ${CMAKE_CURRENT_BINARY_DIR}/litprog.bib
  ${CMAKE_CURRENT_BINARY_DIR}/misc.bib
  ${CMAKE_CURRENT_BINARY_DIR}/html.sty
  ${CMAKE_CURRENT_SOURCE_DIR}/bibnames.sty
  ${CMAKE_CURRENT_SOURCE_DIR}/texnames.sty
  )

add_custom_command(OUTPUT ${nuweb_latex_SRCS}
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/litprog.bib ${CMAKE_CURRENT_BINARY_DIR}/litprog.bib
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/misc.bib ${CMAKE_CURRENT_BINARY_DIR}/misc.bib
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/html.sty ${CMAKE_CURRENT_BINARY_DIR}/html.sty
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/bibnames.sty ${CMAKE_CURRENT_BINARY_DIR}/bibnames.sty
  COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_SOURCE_DIR}/texnames.sty ${CMAKE_CURRENT_BINARY_DIR}/texnames.sty
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/litprog.bib ${CMAKE_CURRENT_SOURCE_DIR}/misc.bib ${CMAKE_CURRENT_SOURCE_DIR}/html.sty
  )

add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/nuweb.pdf
  COMMAND pdflatex ${CMAKE_CURRENT_BINARY_DIR}/nuweb.tex
  COMMAND bibtex nuweb
  COMMAND nuweb -r ${CMAKE_CURRENT_SOURCE_DIR}/nuwebsty.w
  COMMAND nuweb -r ${CMAKE_CURRENT_SOURCE_DIR}/nuweb.w
  COMMAND pdflatex ${CMAKE_CURRENT_BINARY_DIR}/nuweb.tex
  COMMAND pdflatex ${CMAKE_CURRENT_BINARY_DIR}/nuweb.tex
  COMMAND pdflatex ${CMAKE_CURRENT_BINARY_DIR}/nuweb.tex
  DEPENDS nuweb ${nuweb_w_SRCS} ${nuwebsty_w_SRCS} ${nuweb_latex_SRCS}
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
  )
if(NOT TARGET pdf)
  add_custom_target(pdf DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/nuweb.pdf)
endif(NOT TARGET pdf)
add_custom_target(nuweb-pdf DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/nuweb.pdf)

set(nuweb_latex_output
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.aux
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.bbl
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.blg
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.brf
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.log
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.out
  ${CMAKE_CURRENT_BINARY_DIR}/nuweb.toc
  )
if(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")
  list(APPEND nuweb_latex_output ${CMAKE_CURRENT_BINARY_DIR}/bibnames.sty)
endif(NOT "${CMAKE_CURRENT_SOURCE_DIR}" STREQUAL "${CMAKE_CURRENT_BINARY_DIR}")

set_property(DIRECTORY APPEND PROPERTY ADDITIONAL_MAKE_CLEAN_FILES "${nuweb_latex_output}")

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8
