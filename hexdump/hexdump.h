/* ==========================================================================
 * hexdump.h - hexdump.c
 * --------------------------------------------------------------------------
 * Copyright (c) 2013  William Ahern
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
#ifndef HEXDUMP_H
#define HEXDUMP_H

/* Note - this version has the Lua support removed - for the full version,
 * see https://github.com/wahern/hexdump */

/*
 * H E X D U M P  V E R S I O N  I N T E R F A C E S
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define HXD_VERSION HXD_V_REL
#define HXD_VENDOR "william@25thandClement.com"

#define HXD_V_REL 0x20160408
#define HXD_V_ABI 0x20130210
#define HXD_V_API 0x20130412

int hxd_version(void);
const char *hxd_vendor(void);

int hxd_v_rel(void);
int hxd_v_abi(void);
int hxd_v_api(void);


/*
 * H E X D U M P  E R R O R  I N T E R F A C E S
 *
 * Hexdump internal error conditions are returned using regular int objects.
 * System errors are loaded from errno as soon as encountered, and the value
 * returned through the API like internal errors. DO NOT check errno, which
 * may have been overwritten by subsequent error handling code. Internal
 * errors are negative and utilize a simple high-order-byte namespacing
 * protocol. This works because ISO C and POSIX guarantee that all system
 * error codes are positive.
 *
 * hxd_strerror() will forward system errors to strerror(3).
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define HXD_EBASE -(('D' << 24) | ('U' << 16) | ('M' << 8) | 'P')
#define HXD_ERROR(error) ((error) >= XD_EBASE && (error) < XD_ELAST)


enum hxd_errors {
	HXD_EFORMAT = HXD_EBASE,
	/* a compile-time error signaling an invalid format string, format
	   unit, or conversion specification syntax */

	HXD_EDRAINED,
	/* a compile-time error signaling that preceding conversions have
	   already drained the input block */

	HXD_ENOTSUPP,
	/* a compile-time error returned for valid but unsupported
	   conversion specifications */

	HXD_EOOPS,
	/* something horrible happened */

	HXD_ELAST
}; /* enum hxd_errors */

#define hxd_error_t int /* for documentation purposes only */

const char *hxd_strerror(hxd_error_t);


/*
 * H E X D U M P  C O R E  I N T E R F A C E S
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

struct hexdump;

struct hexdump *hxd_open(hxd_error_t *);

void hxd_close(struct hexdump *);

void hxd_reset(struct hexdump *);

#define HXD_BYTEORDER(x)  (0x03 & (x))
#define HXD_NATIVE         0x00
#define HXD_NETWORK        HXD_BIG_ENDIAN
#define HXD_BIG_ENDIAN     0x01
#define HXD_LITTLE_ENDIAN  0x02
#define HXD_NOPADDING      0x04

hxd_error_t hxd_compile(struct hexdump *, const char *, int);

const char *hxd_help(struct hexdump *);

size_t hxd_blocksize(struct hexdump *);

hxd_error_t hxd_write(struct hexdump *, const void *, size_t);

hxd_error_t hxd_flush(struct hexdump *);

size_t hxd_read(struct hexdump *, void *, size_t);


/*
 * H E X D U M P  C O M M O N  F O R M A T S
 *
 * Predefined formats for hexdump(1) -b, -c, -C, -d, -o, -x, and xxd(1) -i.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#define HEXDUMP_b "\"%07.7_ax \" 16/1 \"%03o \" \"\\n\""
#define HEXDUMP_c "\"%07.7_ax \" 16/1 \"%3_c \" \"\\n\""
#define HEXDUMP_C "\"%08.8_ax  \" 8/1 \"%02x \" \"  \" 8/1 \"%02x \"\n" \
                  "\"  |\" 16/1 \"%_p\" \"|\\n\""
#define HEXDUMP_d "\"%07.7_ax \" 8/2 \"  %05u \" \"\\n\""
#define HEXDUMP_o "\"%07.7_ao   \" 8/2 \" %06o \" \"\\n\""
#define HEXDUMP_x "\"%07.7_ax \" 8/2 \"   %04x \" \"\\n\""

#define HEXDUMP_i "\"  \" 12/1? \"0x%02x, \" \"\\n\""


#endif /* HEXDUMP_H */
