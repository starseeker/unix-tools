if(EXISTS ${output_blessed})
  execute_process(
    COMMAND ${test_cmd} -f ${sed_file} ${sed_in}
    OUTPUT_VARIABLE OUTPUT
    ERROR_VARIABLE OUTPUT
    )
  file(WRITE "${output_test}" "${OUTPUT}")
  execute_process(
    COMMAND ${CMAKE_COMMAND} -E compare_files ${output_blessed} ${output_test}
    RESULT_VARIABLE test_not_successful
    )
else(EXISTS ${output_blessed})
  execute_process(
    COMMAND ${test_cmd} -f ${sed_file} ${sed_in}
    OUTPUT_VARIABLE OUTPUT
    ERROR_VARIABLE OUTPUT_QUIET
    )
  if("${OUTPUT}" STREQUAL "")
    set(test_not_successful 0)
  else("${OUTPUT}" STREQUAL "")
    set(test_not_successful 1)
  endif("${OUTPUT}" STREQUAL "")
endif(EXISTS ${output_blessed})

if( test_not_successful )
  message(SEND_ERROR "${output_test} does not match ${output_blessed}!" )
endif( test_not_successful )

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

